 @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Index</div>
                <div class="panel-body">
                    
                     <center> <div > 
                   <a class="btn btn-info" href="factory/create" role="button">New Factory</a>
                </div> </center><br>
 <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>serial no</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Address</th>
                        <th>Workers</th>
                        <th>Action</th>
                        
                    </tr>
                    </thead>


                    <tbody>
                    <?php $slNo = 0; ?>
                    @foreach($factories as $factory)
                        <?php $slNo++ ?>
                    <tr>
                        <td>{!! $slNo !!}</td>
                        <td>{!! $factory->name !!}</td>
                        <td>{!! $factory->location !!}</td>
                        <td>{!! $factory->address !!}</td>
                        <td>{!! $factory->workers !!}</td>
                        <td> 
                        
                        <a href="{{url('/factory/'.$factory->id.'/edit')}}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i>Update</a>

                        {!! Form::open(array('method'=>'DELETE', 'route'=>array('factory.destroy',$factory->id)))!!}

                        {!! Form::submit('Delete', array('class'=>'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure want to Delete?");'))!!}
                                        {!! Form::close()!!}</td>
                    </tr>

                        @endforeach

                    </tbody>
                </table>
                @endsection