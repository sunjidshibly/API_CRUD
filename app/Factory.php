<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
    protected $fillable = [
        'name', 'location', 'address','workers','user_id',
    ];

     public function users()
    {
        return $this->belongsTo('App\User');
    }
}
