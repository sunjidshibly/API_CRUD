@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Factory</div>
                <div class="panel-body">

                                 
                {!! Form::open(['route' => 'factory.store', 'files'=> true]) !!}
                {!! Form::hidden('user_id', Auth::user()->id )!!}
                                    <div class="form-group">
                                        {!! Form::label('name', 'Name') !!}
                                        {!! Form::text('name', null, ['class' => 'form-control','required','autofocus']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('location', 'Location') !!}
                                        {!! Form::text('location', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('address', 'Address') !!}
                                        {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('workers', 'Workers') !!}
                                        {!! Form::number('workers', null, ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
                                {!! Form::reset('Reset', array('class'=>'btn btn-warning')) !!}

                                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
